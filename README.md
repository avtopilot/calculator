SPA web application
=======================================

Prerequisites
---------------------------------------
* .Net core
* node
* npm

Running
--------------------------------------
```bash
sh ./runWebApi.sh //will run WebApi on the 5000 port
sh ./runClient.sh //will run client on the 3000 port
```
Open browser at http://localhost:3000